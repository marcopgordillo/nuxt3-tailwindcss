import { Config } from 'tailwindcss'
import defaultTheme from 'tailwindcss/defaultTheme'
import tailwindTypography from '@tailwindcss/typography'
import daisyUI from 'daisyui'

export default <Config> {
    darkMode: 'class',
    content: [
        "./content/**/*.md",
    ],
    theme: {
    extend: {
        fontFamily: {
            sans: ['"Source Sans Pro"', ...defaultTheme.fontFamily.sans],
        },
    },
    },
    plugins: [
        tailwindTypography,
        daisyUI,
    ],
    daisyUI: {
        themes: ['light', 'dark'],
    },
}